using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reactive.Subjects;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Bot.DiscordRelated.Logging.Rendering;
using Common;
using Common.Localization.Providers;
using Bot.Utilities.DiffMatchPatch;
using Discord;
using Discord.WebSocket;
using DiscordChatExporter.Domain.Discord.Models;
using DiscordChatExporter.Domain.Discord.Models.Common;
using LiteDB;
using Attachment = DiscordChatExporter.Domain.Discord.Models.Attachment;
using Embed = DiscordChatExporter.Domain.Discord.Models.Embed;
using MessageType = DiscordChatExporter.Domain.Discord.Models.MessageType;

namespace Bot.DiscordRelated.Logging {
    public class MessageHistory {
        [Obsolete("Use IMessageHistoryProvider")]
        public MessageHistory() { }

        public class MessageSnapshot {
            public DateTimeOffset EditTimestamp { get; set; }
            public string Value { get; set; } = null!;
        }

        [BsonField("At")] public List<string>? Attachments { get; set; }

        [BsonField("U")] public bool IsHistoryUnavailable { get; set; }

        [BsonId] public string Id { get; internal set; } = null!;

        [BsonField("A")] public ulong AuthorId { get; set; }

        [BsonIgnore] public ulong ChannelId => Convert.ToUInt64(Id.Split(":")[0]);

        [BsonIgnore] public ulong MessageId => Convert.ToUInt64(Id.Split(":")[1]);

        [BsonField("E")] public List<MessageSnapshot> Edits { get; set; } = new List<MessageSnapshot>();

        [BsonIgnore] public bool HistoryExists => Edits.Count != 0;

        [BsonIgnore] public bool HasAttachments => Attachments != null && Attachments.Count != 0;

        [BsonIgnore] private static Regex AttachmentRegex = new Regex(@"(\d+)\/(\d+)\/(.+?)$");

        [BsonIgnore] public ISubject<MessageHistory> SaveRequest = new Subject<MessageHistory>();

        public void Save() {
            SaveRequest.OnNext(this);
        }

        public void AddSnapshot(IMessage message) {
            AddSnapshot(message.EditedTimestamp ?? message.Timestamp, message.Content);
        }

        public void AddSnapshot(DateTimeOffset editTime, string? newContent) {
            newContent ??= "";
            Edits.Add(new MessageSnapshot {
                EditTimestamp = editTime,
                Value = Utilities.DiffMatchPatch.DiffMatchPatch.patch_toText(MessageHistoryService.DiffMatchPatch.patch_make(GetLastContent(), newContent))
            });
        }

        public bool CanFitToEmbed(ILocalizationProvider loc) {
            if (Edits.Count > 20)
                return false;
            var commonCount = HasAttachments ? GetAttachmentsString(false).Result.Length : 0;
            MessageSnapshot? lastSnapshot = null;
            foreach (var edit in GetSnapshots(loc)) {
                if (edit.Value.Length > 1020)
                    return false;
                commonCount += edit.Value.Length + edit.EditTimestamp.ToString().Length;
                if (commonCount > 5500)
                    return false;
                lastSnapshot = edit;
            }

            return commonCount - (lastSnapshot?.EditTimestamp.ToString().Length ?? 0) +
                loc.Get("MessageHistory.LastContent").Format(lastSnapshot?.EditTimestamp).Length <= 5500;
        }

        public SocketGuildUser? GetAuthor() {
            try {
                return Program.Client.GetUser(AuthorId) as SocketGuildUser;
            }
            catch (Exception) {
                return null;
            }
        }

        public string GetLastContent() {
            try {
                return MessageHistoryService.DiffMatchPatch.patch_apply(
                    Edits.SelectMany(s1 => Utilities.DiffMatchPatch.DiffMatchPatch.patch_fromText(s1.Value)).ToList(), "")[0].ToString() ?? "";
            }
            catch (Exception) {
                return "";
            }
        }

        public IEnumerable<MessageSnapshot> GetSnapshots(ILocalizationProvider loc, bool injectDiffsHighlight = false) {
            var snapshots = new List<MessageSnapshot>();
            if (IsHistoryUnavailable) {
                yield return new MessageSnapshot {Value = loc.Get("MessageHistory.PreviousUnavailable"), EditTimestamp = default};
            }

            foreach (var edit in Edits) {
                var last = snapshots.Count == 0 ? "" : snapshots.Last().Value;
                var snapshot = MessageHistoryService.DiffMatchPatch.patch_apply(Utilities.DiffMatchPatch.DiffMatchPatch.patch_fromText(edit.Value), last)[0].ToString() ??
                               "";

                snapshots.Add(new MessageSnapshot {EditTimestamp = edit.EditTimestamp, Value = snapshot});
                if (injectDiffsHighlight && snapshots.Count > 1) {
                    var diffMain = MessageHistoryService.DiffMatchPatch.diff_main(last, snapshot);
                    MessageHistoryService.DiffMatchPatch.diff_cleanupSemantic(diffMain);
                    yield return new MessageSnapshot {EditTimestamp = edit.EditTimestamp, Value = DiffsToHtmlEncode(diffMain)};
                }
                else {
                    yield return snapshots.Last();
                }
            }
        }

        public IEnumerable<EmbedFieldBuilder> GetEditsAsFields(ILocalizationProvider loc) {
            var embedFields = GetSnapshots(loc).Select(messageSnapshot => new EmbedFieldBuilder {
                Name = messageSnapshot.EditTimestamp.ToString(),
                Value = string.IsNullOrWhiteSpace(messageSnapshot.Value) ? loc.Get("MessageHistory.EmptyMessage") : $">>> {messageSnapshot.Value}"
            }).ToList();

            var lastContent = embedFields.Last();
            lastContent.Name = loc.Get("MessageHistory.LastContent").Format(lastContent.Name);

            return embedFields;
        }

        public async Task<string> ExportToHtml(ILocalizationProvider loc, bool injectDiffsHighlight = true) {
            // Create context
            var context = LogExportContext.Create(ChannelId, out var members);
            var stream = new MemoryStream();

            // Render messages
            var renderer = new LogMessageWriter(stream, context, "Dark");
            try {
                var user = ConstructUser(AuthorId);
                var member = Member.CreateForUser(user);
                members.Add(member);

                await renderer.WritePreambleAsync();

                var messageSnapshots = GetSnapshots(loc, injectDiffsHighlight).ToList();
                var hasAttachments = Attachments != null && Attachments.Count != 0;

                // If we have attachments and message edits
                // Then render attachments separately
                if (hasAttachments && messageSnapshots.Count != 1) {
                    await renderer.WriteMessageAsync(new Message("", MessageType.Default, user, DateTimeOffset.MinValue, null, true, "",
                        (await GetExportAttachments()).ToList(), new List<Embed>(), new List<Reaction>(), new List<User>()));
                }

                foreach (var messageSnapshot in messageSnapshots) {
                    foreach (var userMention in GetUserMentions(messageSnapshot.Value)) members.Add(Member.CreateForUser(userMention));

                    await renderer.WriteMessageAsync(new Message(MessageId.ToString(), MessageType.Default, user, messageSnapshot.EditTimestamp,
                        null, false, messageSnapshot.Value,
                        hasAttachments && messageSnapshots.Count == 1 ? (await GetExportAttachments()).ToList() : new List<Attachment>(),
                        new List<Embed>(), new List<Reaction>(), members.Select(member1 => member1.User).ToList()));
                }

                await renderer.WritePostambleAsync();
                try {
                    stream.Position = 0;
                    var readToEndAsync = await new StreamReader(stream).ReadToEndAsync();
                    return HtmlDiffsUnEncode(readToEndAsync);
                }
                finally {
                    await renderer.DisposeAsync();
                }
            }
            finally {
                await renderer.DisposeAsync();
            }
        }

        private static IEnumerable<User> GetUserMentions(string text) {
            foreach (Match? m in Regex.Matches(text, @"(?<=<@|<@!)[0-9]{18}(?=>)", RegexOptions.Multiline))
                if (m != null)
                    yield return ConstructUser(Convert.ToUInt64(m.Value));
        }

        private static User ConstructUser(ulong userId) {
            return ConstructUser(Program.Client.GetUser(userId));
        }

        private static User ConstructUser(SocketUser user) {
            return new User(user.Id.ToString(), user.IsBot, user.DiscriminatorValue, user.Username, user.AvatarId);
        }

        private static string DiffsToHtmlEncode(List<Diff> diffs) {
            var html = new StringBuilder();
            foreach (var aDiff in diffs) {
                // ReSharper disable once SwitchStatementMissingSomeEnumCasesNoDefault
                switch (aDiff.Operation) {
                    case Operation.Insert:
                        html.Append("࢔")
                            .Append(aDiff.Text)
                            .Append("࢕");
                        break;
                    case Operation.Delete:
                        html.Append("࢖")
                            .Append(aDiff.Text)
                            .Append("ࢗ");
                        break;
                    case Operation.Equal:
                        html.Append(aDiff.Text);
                        break;
                }
            }

            return html.ToString();
        }

        // ReSharper disable once UnusedMethodReturnValue.Local
        private static bool ParseAttachment(string s, out string id, out string fileName) {
            try {
                var match = AttachmentRegex.Match(s);
                id = match.Groups[2].Value;
                fileName = match.Groups[3].Value;
                return true;
            }
            catch (Exception) {
                id = "";
                fileName = "";
                return false;
            }
        }

        public async Task<Attachment[]> GetExportAttachments(bool needFileSize = true) {
            return await Task.WhenAll(Attachments.Select(async s => {
                ParseAttachment(s, out var id, out var fileName);
                long fileSize = 0;
                try {
                    if (needFileSize && !Attachment.ImageFileExtensions.Contains(Path.GetExtension(fileName), StringComparer.OrdinalIgnoreCase)) {
                        var request = WebRequest.CreateHttp(s);
                        request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1";
                        request.Method = "HEAD";
                        using var response = await request.GetResponseAsync();
                        fileSize = response.ContentLength;
                    }
                }
                catch (Exception) {
                    // ignored
                }

                var attachment = new Attachment(id, s, fileName, null, null, FileSize.FromBytes(fileSize));
                return attachment;
            }));
        }

        public async Task<string> GetAttachmentsString(bool needFileSize = true) {
            return string.Join("\n",
                (await GetExportAttachments(needFileSize)).Select(attachment =>
                    $"[{attachment.FileName} ({attachment.FileSize.ToString()})]({attachment.Url})"));
        }

        private static string HtmlDiffsUnEncode(string encoded) {
            return encoded.Replace("࢔", "<span style=\"background:DarkGreen;\">").Replace("࢕", "</span>")
                          .Replace("࢖", "<span class=\"removed\">").Replace("ࢗ", "</span>");
        }

        public async Task<IUserMessage?> GetRealMessage() {
            try {
                var textChannel = (ITextChannel) Program.Client.GetChannel(ChannelId);
                var messageAsync = await textChannel.GetMessageAsync(MessageId)!;
                return messageAsync as IUserMessage;
            }
            catch (Exception) {
                return null;
            }
        }
    }
}